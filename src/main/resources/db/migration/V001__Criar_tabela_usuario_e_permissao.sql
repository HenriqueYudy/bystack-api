CREATE TABLE usuario(
	id_usuario		SERIAL 			PRIMARY KEY,
	login			VARCHAR(100)	NOT NULL,
	senha			VARCHAR(255)	NOT NULL,
	nome			VARCHAR(150)	NOT NULL
);

CREATE TABLE permissao(
	id_permissao 	SERIAL 			PRIMARY KEY,
	descricao		VARCHAR(30)		NOT NULL		
);

CREATE TABLE usuario_permissao(
	id_usuario_permissao		SERIAL 		PRIMARY KEY,
	id_usuario					bigint		NOT NULL,
	id_permissao				bigint		NOT NULL,
	constraint fk_usrperm_usuario foreign key (id_usuario) references usuario (id_usuario),
	constraint fk_usrperm_permissao foreign key (id_permissao) references permissao (id_permissao)
);