CREATE TABLE endereco(
	id_endereco 						SERIAL 			PRIMARY KEY,
	bairro				VARCHAR(200)	NOT NULL,
	logradouro			VARCHAR(200)	NOT NULL,
	numero				INTEGER 		NOT NULL,
	cidade				bigint			NOT NULL,
	constraint fk_endereco_cidade foreign key (cidade) references cidade (id_cidade)
);

insert into endereco values (1, 'Eneias' ,'Rua Jose Martins Gallego', 420, 1);
insert into endereco values (2, 'Centro' ,'Av. Faria Pereira', 1090, 1);
insert into endereco values (3, 'Centro' ,'Av. Rui Barbosa', 324, 1);
insert into endereco values (4, 'Centro' ,'Av Governador Valadares', 3456, 1);
insert into endereco values (5, 'Centro' ,'Rua Bernado Guimarães', 3467, 1);
insert into endereco values (6, 'Constantino' ,'Av. João Alves do Nascimento', 2786, 1);