CREATE TABLE nota_fiscal(
	id_nota_fiscal 					SERIAL 			PRIMARY KEY,
	movimentacao_saida				bigint			,
	movimentacao_entrada			bigint          ,
	constraint fk_nota_mov_saida foreign key (movimentacao_saida) references movimentacao_saida (id_movimentacao_saida),
	constraint fk_nota_mov_entrada foreign key (movimentacao_entrada) references movimentacao_entrada (id_movimentacao_entrada)
)
