CREATE TABLE cidade(
	id_cidade 			SERIAL 			PRIMARY KEY,
	nome				VARCHAR(100)	NOT NULL
);

insert into cidade values (1, 'Patrocinio');
insert into cidade values (2, 'Paracatu');
insert into cidade values (3, 'Uberlandia');
insert into cidade values (4, 'Patos de Minas');
insert into cidade values (5, 'Araguari');

