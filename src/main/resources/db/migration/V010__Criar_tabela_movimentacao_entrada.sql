CREATE TABLE movimentacao_entrada(
	id_movimentacao_entrada 		SERIAL 			PRIMARY KEY,
	quantidade		   			    INTEGER			NOT NULL,
	valor_total						DECIMAL			NOT NULL,
	data_movimentacao			    DATE			NOT NULL,
	produto_preco					bigint			NOT NULL,
	fornecedor						bigint			NOT NULL,
	constraint fk_mov_entrada_produto foreign key (produto_preco) references produto_preco (id_produto_preco),
	constraint fk_mov_entrada_fornecedor foreign key (fornecedor) references fornecedor (id_fornecedor)
)