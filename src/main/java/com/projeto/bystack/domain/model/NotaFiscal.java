package com.projeto.bystack.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nota_fiscal")
public class NotaFiscal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_nota_fiscal")
	private long id;

	@ManyToOne
	@JoinColumn(name = "movimentacao_saida")
	private MovimentacaoSaida movimentacao_saida;

	@ManyToOne
	@JoinColumn(name = "movimentacao_entrada")
	private MovimentacaoEntrada movimentacao_entrada;

	public long getId() {
		return id;
	}

	public MovimentacaoSaida getMovimentacao_saida() {
		return movimentacao_saida;
	}

	public MovimentacaoEntrada getMovimentacao_entrada() {
		return movimentacao_entrada;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMovimentacao_saida(MovimentacaoSaida movimentacao_saida) {
		this.movimentacao_saida = movimentacao_saida;
	}

	public void setMovimentacao_entrada(MovimentacaoEntrada movimentacao_entrada) {
		this.movimentacao_entrada = movimentacao_entrada;
	}

	@Override
	public String toString() {
		return "NotaFiscal [id=" + id + ", movimentacao_saida=" + movimentacao_saida + ", movimentacao_entrada="
				+ movimentacao_entrada + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscal other = (NotaFiscal) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
