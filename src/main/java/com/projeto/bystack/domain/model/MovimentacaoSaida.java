package com.projeto.bystack.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "movimentacao_saida")
public class MovimentacaoSaida {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_movimentacao_saida")
	private long id;

	@Column(name = "quantidade")
	private int quantidade;

	@Column(name = "desconto")
	private BigDecimal desconto;

	@Column(name = "valor_pgto")
	private BigDecimal valor_pgto;

	@Column(name = "valor_total")
	private BigDecimal valor_total;

	@Column(name = "data_movimentacao")
	private LocalDate data_movimentacao;

	@ManyToOne
	@JoinColumn(name = "produto_preco")
	private ProdutoPreco produto_preco;

	@ManyToOne
	@JoinColumn(name = "cliente")
	private Cliente cliente;

	public long getId() {
		return id;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}

	public BigDecimal getValor_pgto() {
		return valor_pgto;
	}

	public BigDecimal getValor_total() {
		return valor_total;
	}

	public LocalDate getData_movimentacao() {
		return data_movimentacao;
	}

	public ProdutoPreco getProduto_preco() {
		return produto_preco;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public void setValor_pgto(BigDecimal valor_pgto) {
		this.valor_pgto = valor_pgto;
	}

	public void setValor_total(BigDecimal valor_total) {
		this.valor_total = valor_total;
	}

	public void setData_movimentacao(LocalDate data_movimentacao) {
		this.data_movimentacao = data_movimentacao;
	}

	public void setProduto_preco(ProdutoPreco produto_preco) {
		this.produto_preco = produto_preco;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "MovimentacaoSaida [id=" + id + ", quantidade=" + quantidade + ", desconto=" + desconto + ", valor_pgto="
				+ valor_pgto + ", valor_total=" + valor_total + ", data_movimentacao=" + data_movimentacao
				+ ", produto_preco=" + produto_preco + ", cliente=" + cliente + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovimentacaoSaida other = (MovimentacaoSaida) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
