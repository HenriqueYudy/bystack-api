package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.Cidade;
import com.projeto.bystack.domain.repository.CidadeRepository;

@RestController
@RequestMapping(value = "/security")
public class CidadeResource {

	@Autowired
	private CidadeRepository cidadeRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/cidade/add")
	public ResponseEntity<Cidade> saveCidade(@Valid @RequestBody Cidade cidade) {

		Cidade newCidade = cidadeRepository.save(cidade);

		return ResponseEntity.status(HttpStatus.CREATED).body(newCidade);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path =  "/cidade/get/{id}")
	public ResponseEntity<Cidade> getCidade(@PathVariable("id") long id) {

		Cidade resultCidade = cidadeRepository.findOne(id);

		if (resultCidade == null) {
			return new ResponseEntity<Cidade>(HttpStatus.NOT_FOUND);
		} 
			return new ResponseEntity<Cidade>(resultCidade, HttpStatus.OK);
		

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path  = "/cidade/getall")
	public ResponseEntity<List<Cidade>> getAllCidade(){
		
		List<Cidade> lstCidade = cidadeRepository.findAll();
		
		if(lstCidade.isEmpty()) {
			return new ResponseEntity<List<Cidade>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Cidade>>(lstCidade , HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/cidade/update/{id}")
	public ResponseEntity<Cidade> updateCidade(@PathVariable("id") long id, @RequestBody Cidade cidade){
		
		Cidade updateCidade = cidadeRepository.findOne(id);
		
		if(updateCidade == null ) {
			return new ResponseEntity<Cidade>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(cidade, updateCidade, "id");
		cidadeRepository.save(updateCidade);
		return new ResponseEntity<Cidade>(updateCidade, HttpStatus.OK);
		
		
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path =  "/cidade/delete/{id}") 
	public ResponseEntity<Cidade> deleteCidade(@PathVariable("id") long id){
		
		Cidade deleteCidade = cidadeRepository.findOne(id);
		
		if(deleteCidade == null) { 
			return new ResponseEntity<Cidade>(HttpStatus.NOT_FOUND);
		}
		
		cidadeRepository.delete(deleteCidade);
		return new ResponseEntity<Cidade>(HttpStatus.NO_CONTENT);
	
	}

}
