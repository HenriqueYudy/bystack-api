package com.projeto.bystack.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.MovimentacaoEntrada;
import com.projeto.bystack.domain.repository.MovimentacaoEntradaRepository;

@RestController
@RequestMapping(value = "/security")
public class MovimentacaoEntradaResource {

	@Autowired
	private MovimentacaoEntradaRepository movimentacaoEntradaRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/movimentacao_entrada/add")
	public ResponseEntity<MovimentacaoEntrada> saveMovimentacaoEntrada(
			@Valid @RequestBody MovimentacaoEntrada movimentacaoEntrada) {

		MovimentacaoEntrada newMovEntrada = movimentacaoEntradaRepository.save(movimentacaoEntrada);

		return ResponseEntity.status(HttpStatus.CREATED).body(newMovEntrada);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/movimentacao_entrada/get/{id}")
	public ResponseEntity<MovimentacaoEntrada> getMovimentacaoEntrada(@PathVariable("id") long id) {

		MovimentacaoEntrada movEntrada = movimentacaoEntradaRepository.findOne(id);

		if (movEntrada == null) {
			return new ResponseEntity<MovimentacaoEntrada>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<MovimentacaoEntrada>(movEntrada, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/movimentacao_entrada/getall/{id}")
	public ResponseEntity<List<MovimentacaoEntrada>> getAllMovimentacao_entrada() {

		List<MovimentacaoEntrada> lstMovEntrada = movimentacaoEntradaRepository.findAll();

		if (lstMovEntrada.isEmpty()) {
			return new ResponseEntity<List<MovimentacaoEntrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<MovimentacaoEntrada>>(lstMovEntrada, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/movimentacao_entrada/update/{id}")
	public ResponseEntity<MovimentacaoEntrada> updateMovimentacaoEntrada(@PathVariable("id") long id,
			MovimentacaoEntrada movimentacaoEntrada) {

		MovimentacaoEntrada updateMovEntrada = movimentacaoEntradaRepository.findOne(id);

		if (updateMovEntrada == null) {
			return new ResponseEntity<MovimentacaoEntrada>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(movimentacaoEntrada, updateMovEntrada, "id");
		movimentacaoEntradaRepository.save(updateMovEntrada);
		return new ResponseEntity<MovimentacaoEntrada>(updateMovEntrada, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/movimentacao_entrada/delete/{id}")
	public ResponseEntity<MovimentacaoEntrada> deleteMovimentacaoEntrada(@PathVariable("id") long id) {

		MovimentacaoEntrada deleteMovEntrada = movimentacaoEntradaRepository.findOne(id);

		if (deleteMovEntrada == null) {
			return new ResponseEntity<MovimentacaoEntrada>(HttpStatus.NOT_FOUND);
		}

		movimentacaoEntradaRepository.delete(deleteMovEntrada);
		return new ResponseEntity<MovimentacaoEntrada>(HttpStatus.NO_CONTENT);
	}

}
