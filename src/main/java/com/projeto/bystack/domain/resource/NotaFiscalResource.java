package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.NotaFiscal;
import com.projeto.bystack.domain.repository.NotaFiscalRepository;

@RestController
@RequestMapping(value = "/security")
public class NotaFiscalResource {

	@Autowired
	private NotaFiscalRepository notaFiscalRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/nota_fiscal/add")
	public ResponseEntity<NotaFiscal> saveNotaFiscal(@Valid @RequestBody NotaFiscal notaFiscal) {

		NotaFiscal newNotaFiscal = notaFiscalRepository.save(notaFiscal);

		return ResponseEntity.status(HttpStatus.CREATED).body(notaFiscal);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/nota_fiscal/get/{id}")
	public ResponseEntity<NotaFiscal> getNotaFiscal(@PathVariable("id") long id) {

		NotaFiscal getNotaFiscal = notaFiscalRepository.findOne(id);

		if (getNotaFiscal == null) {
			return new ResponseEntity<NotaFiscal>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<NotaFiscal>(getNotaFiscal, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/nota_fiscal/getall")
	public ResponseEntity<List<NotaFiscal>> getAllNotaFiscal() {

		List<NotaFiscal> lstNotaFiscal = notaFiscalRepository.findAll();

		if (lstNotaFiscal.isEmpty()) {
			return new ResponseEntity<List<NotaFiscal>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<NotaFiscal>>(lstNotaFiscal, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/nota_fiscal/update/{id}")
	public ResponseEntity<NotaFiscal> updateNotaFiscal(@PathVariable("id") long id, NotaFiscal notaFiscal) {

		NotaFiscal updateNotaFiscal = notaFiscalRepository.findOne(id);

		if (updateNotaFiscal == null) {
			return new ResponseEntity<NotaFiscal>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(notaFiscal, updateNotaFiscal, "id");
		notaFiscalRepository.save(updateNotaFiscal);
		return new ResponseEntity<NotaFiscal>(updateNotaFiscal, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/nota_fiscal/delete/{id}")
	public ResponseEntity<NotaFiscal> deleteNotaFiscal(@PathVariable("id") long id) {

		NotaFiscal deleteNotaFiscal = notaFiscalRepository.findOne(id);

		if (deleteNotaFiscal == null) {
			return new ResponseEntity<NotaFiscal>(HttpStatus.NOT_FOUND);
		}

		notaFiscalRepository.delete(deleteNotaFiscal);
		return new ResponseEntity<NotaFiscal>(HttpStatus.NO_CONTENT);
	}

}
