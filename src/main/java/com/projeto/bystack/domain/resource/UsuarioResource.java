package com.projeto.bystack.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.Usuario;
import com.projeto.bystack.domain.repository.UsuarioRepository;

@RestController
@RequestMapping(value = "/security")
public class UsuarioResource {
	
	@Autowired
	private UsuarioRepository usuarioRepository;


	@CrossOrigin(origins = "*")
	@PostMapping(path = "/usuario/add")
	public ResponseEntity<Usuario> saveUsuario(@Valid @RequestBody Usuario usuario){
		Usuario newUsuario = usuarioRepository.save(usuario);
		return ResponseEntity.status(HttpStatus.CREATED).body(newUsuario);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/usuario/get/{id}") 
	public ResponseEntity<Usuario> getUsuario(@PathVariable("id") long id ){
		
		Usuario getUsuario = usuarioRepository.findOne(id);
		
		if(getUsuario == null) {
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Usuario>(getUsuario, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/usuario/getall")
	public ResponseEntity<List<Usuario>> getAllUsuario(){
		
		List<Usuario> lstUsuario = usuarioRepository.findAll();
		
		if(lstUsuario.isEmpty()) {
			return new ResponseEntity<List<Usuario>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Usuario>>(lstUsuario, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/usuario/update/{id}")
	public ResponseEntity<Usuario> updateUsuario(@PathVariable("id") long id, @RequestBody Usuario usuario){
		
		Usuario updateUsuario = usuarioRepository.findOne(id);
		
		if(updateUsuario == null) {
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		
		BeanUtils.copyProperties(usuario, updateUsuario, "id");
		usuarioRepository.save(updateUsuario);
		return new ResponseEntity<Usuario>(updateUsuario, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/usuario/delete/{id}")
	public ResponseEntity<Usuario> deleteUsuario(@PathVariable("id") long id){
		
		Usuario deleteUsuario = usuarioRepository.findOne(id);
		
		if(deleteUsuario == null) {
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		usuarioRepository.delete(deleteUsuario);
		return new ResponseEntity<Usuario>(HttpStatus.NO_CONTENT);
		
	}
}
