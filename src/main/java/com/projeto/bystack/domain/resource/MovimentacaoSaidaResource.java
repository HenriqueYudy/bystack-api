package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.MovimentacaoSaida;
import com.projeto.bystack.domain.repository.MovimentacaoSaidaRepository;

@RestController
@RequestMapping(value = "/security")
public class MovimentacaoSaidaResource {

	@Autowired
	private MovimentacaoSaidaRepository movimentacaoSaidaRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/movimentacao_saida/add")
	public ResponseEntity<MovimentacaoSaida> saveMovimentacaoSaida(
			@Valid @RequestBody MovimentacaoSaida movimentacaoSaida) {

		MovimentacaoSaida newMovimentacaoSaida = movimentacaoSaidaRepository.save(movimentacaoSaida);
		return ResponseEntity.status(HttpStatus.CREATED).body(newMovimentacaoSaida);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/movimentacao_saida/get/{id}")
	public ResponseEntity<MovimentacaoSaida> getMovimentacaoSaida(@PathVariable("id") long id) {

		MovimentacaoSaida getMovimentacaoSaida = movimentacaoSaidaRepository.findOne(id);

		if (getMovimentacaoSaida == null) {
			return new ResponseEntity<MovimentacaoSaida>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<MovimentacaoSaida>(getMovimentacaoSaida, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/movimentacao_saida/getall")
	public ResponseEntity<List<MovimentacaoSaida>> getAllMovimentacaoSaida() {

		List<MovimentacaoSaida> lstAllMovimentacaoSaida = movimentacaoSaidaRepository.findAll();

		if (lstAllMovimentacaoSaida.isEmpty()) {
			return new ResponseEntity<List<MovimentacaoSaida>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<MovimentacaoSaida>>(lstAllMovimentacaoSaida, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/movimentacao_saida/update/{id}")
	public ResponseEntity<MovimentacaoSaida> updateMovimentacaoSaida(@PathVariable("id") long id,
			MovimentacaoSaida movimentacaoSaida) {

		MovimentacaoSaida updateMovimentacaoSaida = movimentacaoSaidaRepository.findOne(id);

		if (updateMovimentacaoSaida == null) {
			return new ResponseEntity<MovimentacaoSaida>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(movimentacaoSaida, updateMovimentacaoSaida, "id");
		movimentacaoSaidaRepository.save(updateMovimentacaoSaida);
		return new ResponseEntity<MovimentacaoSaida>(updateMovimentacaoSaida, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/movimentacao_saida/delete/{id}")
	public ResponseEntity<MovimentacaoSaida> deleteMovimentacaoSaida(@PathVariable("id") long id) {

		MovimentacaoSaida deleteMovimentacao = movimentacaoSaidaRepository.findOne(id);

		if (deleteMovimentacao == null) {
			return new ResponseEntity<MovimentacaoSaida>(HttpStatus.NOT_FOUND);
		}
		movimentacaoSaidaRepository.delete(deleteMovimentacao);
		return new ResponseEntity<MovimentacaoSaida>(HttpStatus.NO_CONTENT);
	}

}
