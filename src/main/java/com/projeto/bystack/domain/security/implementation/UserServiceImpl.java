package com.projeto.bystack.domain.security.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.projeto.bystack.domain.model.Usuario;
import com.projeto.bystack.domain.repository.UsuarioRepository;

import scala.annotation.varargs;

@Service(value = "userService")
public class UserServiceImpl {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	public Usuario save(Usuario user) {
		Usuario newUsuario = new Usuario();
		
		newUsuario.setNome(user.getNome());
		newUsuario.setLogin(user.getLogin());
		newUsuario.setSenha(bCryptPasswordEncoder.encode(user.getSenha()));
		
		return usuarioRepository.save(newUsuario);
	}
	
	public List<Usuario> findAll(){
		List<Usuario> list = new ArrayList<>();
		
		usuarioRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	 
}
