package com.projeto.bystack.domain.security;

import java.util.List;

import com.projeto.bystack.domain.model.Usuario;

public interface UserService {
	
	Usuario save(Usuario user);

	List<Usuario> findAll();

	void delete(long id);

	Usuario findOne(String username);

	Usuario updatePass(long id, Usuario user);

	Usuario findById(long id);

}
