package com.projeto.bystack.domain.security;

public class Constants {
	
	public static final long ACESS_TOKEN_VALIDATY_SECONDS = 5 * 60 * 60;
	public static final String SIGNING_KEY = "banana@d1d2d3";
	public static final String TKOEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String AUTHORITIES_KEY = "scopes";

}
