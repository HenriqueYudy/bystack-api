package com.projeto.bystack.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projeto.bystack.domain.model.MovimentacaoSaida;

@Repository
public interface MovimentacaoSaidaRepository extends JpaRepository<MovimentacaoSaida, Long> {

}
